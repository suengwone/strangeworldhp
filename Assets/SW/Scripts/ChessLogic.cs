﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess;


// 오브젝트의 말마다 특성 부여
// 바닥 오브젝트랑 보드 배열의 포지션 맞추기
public class ChessLogic : MonoBehaviour
{
    Dictionary<GameObject, ChessProp> PieceW;
    Dictionary<GameObject, ChessProp> PieceB;
    Transform ChessBoard;
    Transform ChesspieceW;
    Transform ChesspieceB;
    Vector3[,] position = new Vector3[8,8];


    // Start is called before the first frame update
    void Start()
    {
        ChessBoard = gameObject.transform.GetChild(0);
        ChesspieceW = gameObject.transform.GetChild(1);
        ChesspieceB = gameObject.transform.GetChild(2);

        for(int i=0;i<16;i++)
        {
            ChessProp propW = new ChessProp();
            propW.currentPos = ChesspieceW.GetChild(i).position;
            PieceW.Add(ChesspieceW.GetChild(i).gameObject, propW);

            ChessProp propB = new ChessProp();
            propB.currentPos = ChesspieceB.GetChild(i).position;
            PieceB.Add(ChesspieceB.GetChild(i).gameObject, propB);
        }

        for(int i=0;i<8;i++)
        {
            for(int j=0;j<8;j++)
            {
                position[j, i] = ChessBoard.GetChild(i).GetChild(j).position;
            }
        }
    }


    // Update is called once per frame
    void Update()
    {

    }


    // 잡으면 없어지기
    private void OnCollisionEnter(Collision collision)
    {
        // 상대가 자기와 다른 팀이라면

        Destroy(collision.gameObject);
    }
}

struct ChessProp
{
    public Vector3 currentPos;
    public bool Moved { get { return false; } set { Moved = value; } }
    public List<Vector3> possibleMoves { get; set; }
}
