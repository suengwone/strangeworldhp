﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    enum MoveType
    {
        Pawn_Double_Move,
        Normal_Move,
    }

    class Move
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public MoveType Type { get; private set; }

        // x는 숫자(0~7), y는 숫자(0~7)로 받고 영어로 바꿈
        // (x,y) 보드의 자식중에 x번째 자식을 받고 그 안에서 y번째 자식을 받음
        public Move(int x, int y, MoveType type)
        {
            X = x;
            Y = y;
            Type = type;
        }
    }
}

