﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    enum PlayerType
    {
        White,
        Black
    }

    enum PieceType
    {
        Pawn,
        Rook,
        Knight,
        Bishop,
        Queen,
        King
    }

    enum PieceState
    {
        Not_Moved,
        Moved
    }

    class Piece
    {
        public PieceType Type { get; private set; }
        public PieceState State { get; private set; } = PieceState.Not_Moved;
        public List<Move> PossibleMoves { get; private set; } = new List<Move>(20);
        public PlayerType Player { get; private set; }

        public Piece(PieceType pieceType, PlayerType player)
        {
            Type = pieceType;
            Player = player;
        }
        
        // 승급
        /*public void Promote(pieceType type)
        {
            Debug.Assert(Type == pieceType.Pawn, "You cannot use this method except for Pawn");
            Debug.Assert(Type != pieceType.Pawn, "Pawn cannot promote to Pawn.");

            Type = type;
        }*/

        public void SetMoved()
        {
            State = PieceState.Moved;
        }

        public Move GetMoveOrNull(int x, int y)
        {
            foreach(Move move in PossibleMoves)
            {
                if(x==move.X &&  y==move.Y)
                {
                    return move;
                }
            }
            return null;
        }
    }

}
