﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    enum GameState
    { 
        NORMAL,
        CHECK,
        CHECKMATE,
        STALEMATE
    }

    class Board
    {
        public GameState State { get; private set; } = GameState.NORMAL;
        public static Piece[,] mMap = new Piece[8, 8];
        public PlayerType Turn { get; private set; } = PlayerType.Black;

        public void ToggleTurn()
        {
            if (Turn == PlayerType.Black)
            {
                Turn = PlayerType.White;
            }
            else
            {
                Turn = PlayerType.Black;
            }
        }
        public Piece GetPiece(int x, int y)
        {
            return mMap[y, x];
        }

        public void SetGameState()
        {
            if (ValidateIfCheck(mMap, Turn))
            {
                State = GameState.CHECK;
                if (ValidateIfCheckmate(mMap, Turn))
                {
                    State = GameState.CHECKMATE;
                }
            }
            else
            {
                State = GameState.NORMAL;
                if (ValidateIfStalemate(mMap, Turn))
                {
                    State = GameState.STALEMATE;
                }
            }
        }

        private bool ValidateIfCheck(Piece[,] map, PlayerType player)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Piece piece = map[i, j];
                    if (piece != null && piece.Player != player)
                    {
                        List<Move> moves = piece.PossibleMoves;
                        foreach (Move move in moves)
                        {
                            int x = move.X;
                            int y = move.Y;
                            Piece king = map[y, x];
                            if (king != null && king.Type == PieceType.King && king.Player != piece.Player)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        private bool ValidateIfCheckmate(Piece[,] map, PlayerType player)
        {
            if (CountPossibleMoves(player) == 0)
            {
                return true;
            }
            else if (!SimulateIfCheckCanbeSolved(player))
            {
                return true;
            }
            return false;
        }
        private bool ValidateIfStalemate(Piece[,] Map, PlayerType player)
        {
            if (CountPossibleMoves(player) == 0)
            {
                return true;
            }
            else if (SimulateIfAllPossibleMovesMakeCheck(player))
            {
                return true;
            }
            return false;
        }
        public Move GetMoveInPossibleMovesOrNull(int xTo, int yTo, Piece piece)
        {
            return piece.GetMoveOrNull(xTo, yTo);
        }

        public void NormalMove(int xFrom, int yFrom, int xTo, int yTo)
        {
            NormalMove(xFrom, yFrom, xTo, yTo, mMap);
        }
        private void NormalMove(int xFrom, int yFrom, int xTo, int yTo, Piece[,] map)
        {
            map[yTo, xTo] = map[yFrom, xFrom];
            map[yFrom, xFrom] = null;
            map[yTo, xTo].SetMoved();
        }

        public void PawnDoubleMove(int xFrom, int yFrom, int xTo, int yTo)
        {
            PawnDoubleMove(xFrom, yFrom, xTo, yTo, mMap);
        }
        private void PawnDoubleMove(int xFrom, int yFrom, int xTo, int yTo, Piece[,] map)
        {
            map[yTo, xTo] = map[yFrom, xFrom];
            map[yFrom, xFrom] = null;
        }
        public void CalculateMoves()
        {
            CalculateMoves(mMap);
        }
        private void CalculateMoves(Piece[,] map)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Piece piece = map[i, j];
                    if (piece == null) continue;
                    switch (piece.Type)
                    {
                        case PieceType.Pawn:
                            CalculatePawnMoves(piece, j, i, map);
                            break;
                        case PieceType.Rook:
                            CalculateRookMoves(piece, j, i, map);
                            break;
                        case PieceType.Knight:
                            CalculateKnightMoves(piece, j, i, map);
                            break;
                        case PieceType.Bishop:
                            CalculateBishopMoves(piece, j, i, map);
                            break;
                        case PieceType.Queen:
                            CalculateQueenMoves(piece, j, i, map);
                            break;
                        case PieceType.King:
                            CalculateKingMoves(piece, j, i, map);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void CalculatePawnMoves(Piece pawn, int x, int y, Piece[,] map)
        {
            List<Move> moves = pawn.PossibleMoves;
            moves.Clear();
            if (pawn.Player == PlayerType.White)
            {
                if (pawn.State == PieceState.Not_Moved)
                {
                    if (y > 1 && map[y - 2, x] == null)
                    {
                        moves.Add(new Move(x, y - 2, MoveType.Pawn_Double_Move));
                    }
                }
                if (y > 0 && map[y - 1, x] == null)
                {
                    moves.Add(new Move(x, y - 1, MoveType.Normal_Move));
                }
                if (x > 0 && y > 0 && map[y - 1, x - 1] != null && map[y - 1, x - 1].Player != pawn.Player)
                {
                    moves.Add(new Move(x - 1, y - 1, MoveType.Normal_Move));
                }
                if (x < 7 && y > 0 && map[y - 1, x + 1] != null && map[y - 1, x + 1].Player != pawn.Player)
                {
                    moves.Add(new Move(x + 1, y - 1, MoveType.Normal_Move));
                }
            }
            else
            {
                if (pawn.State == PieceState.Not_Moved)
                {
                    if (y < 6 && map[y + 2, x] == null)
                    {
                        moves.Add(new Move(x, y + 2, MoveType.Pawn_Double_Move));
                    }
                }
                if (y < 7 && map[y + 1, x] == null)
                {
                    moves.Add(new Move(x, y + 1, MoveType.Normal_Move));
                }
                if (x > 0 && y < 7 && map[y + 1, x - 1] != null && map[y + 1, x - 1].Player != pawn.Player)
                {
                    moves.Add(new Move(x - 1, y + 1, MoveType.Normal_Move));
                }
                if (x < 7 && y < 7 && map[y + 1, x + 1] != null && map[y + 1, x + 1].Player != pawn.Player)
                {
                    moves.Add(new Move(x + 1, y + 1, MoveType.Normal_Move));
                }
            }
        }

        private void CalculateRookMoves(Piece rook, int x, int y, Piece[,] map)
        {
            List<Move> moves = rook.PossibleMoves;
            moves.Clear();
            int xx = x;
            int yy = y;
            while (true)
            {
                xx--;
                if (xx >= 0)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != rook.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == rook.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                xx++;
                if (xx <= 7)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != rook.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == rook.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                yy--;
                if (yy >= 0)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != rook.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == rook.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                yy++;
                if (yy <= 7)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != rook.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == rook.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        private void CalculateKnightMoves(Piece knight, int x, int y, Piece[,] map)
        {
            List<Move> moves = knight.PossibleMoves;
            moves.Clear();
            if (x - 2 >= 0)
            {
                if (y - 1 >= 0)
                {
                    if (map[y - 1, x - 2] == null || map[y - 1, x - 2].Player != knight.Player)
                    {
                        moves.Add(new Move(x - 2, y - 1, MoveType.Normal_Move));
                    }
                }
                if (y + 1 <= 7)
                {
                    if (map[y + 1, x - 2] == null || map[y + 1, x - 2].Player != knight.Player)
                    {
                        moves.Add(new Move(x - 2, y + 1, MoveType.Normal_Move));
                    }
                }
            }
            if (x + 2 <= 7)
            {
                if (y - 1 >= 0)
                {
                    if (map[y - 1, x + 2] == null || map[y - 1, x + 2].Player != knight.Player)
                    {
                        moves.Add(new Move(x + 2, y - 1, MoveType.Normal_Move));
                    }
                }
                if (y + 1 <= 7)
                {
                    if (map[y + 1, x + 2] == null || map[y + 1, x + 2].Player != knight.Player)
                    {
                        moves.Add(new Move(x + 2, y + 1, MoveType.Normal_Move));
                    }
                }
            }
            if (y - 2 >= 0)
            {
                if (x - 1 >= 0)
                {
                    if (map[y - 2, x - 1] == null || map[y - 2, x - 1].Player != knight.Player)
                    {
                        moves.Add(new Move(x - 1, y - 2, MoveType.Normal_Move));
                    }
                }
                if (x + 1 <= 7)
                {
                    if (map[y - 2, x + 1] == null || map[y - 2, x + 1].Player != knight.Player)
                    {
                        moves.Add(new Move(x + 1, y - 2, MoveType.Normal_Move));
                    }
                }
            }
            if (y + 2 <= 7)
            {
                if (x - 1 >= 0)
                {
                    if (map[y + 2, x - 1] == null || map[y + 2, x - 1].Player != knight.Player)
                    {
                        moves.Add(new Move(x - 1, y + 2, MoveType.Normal_Move));
                    }
                }
                if (x + 1 <= 7)
                {
                    if (map[y + 2, x + 1] == null || map[y + 2, x + 1].Player != knight.Player)
                    {
                        moves.Add(new Move(x + 1, y + 2, MoveType.Normal_Move));
                    }
                }
            }
        }
        private void CalculateBishopMoves(Piece bishop, int x, int y, Piece[,] map)
        {
            List<Move> moves = bishop.PossibleMoves;
            moves.Clear();
            int xx = x;
            int yy = y;
            while (true)
            {
                xx--;
                yy--;
                if (xx >= 0 && yy >= 0)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != bishop.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == bishop.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                xx++;
                yy++;
                if (xx <= 7 && yy <= 7)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != bishop.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == bishop.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                xx--;
                yy++;
                if (xx >= 0 && yy <= 7)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != bishop.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == bishop.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                xx++;
                yy--;
                if (xx <= 7 && yy >= 0)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != bishop.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == bishop.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        private void CalculateQueenMoves(Piece queen, int x, int y, Piece[,] map)
        {
            List<Move> moves = queen.PossibleMoves;
            moves.Clear();
            int xx = x;
            int yy = y;
            while (true)
            {
                xx--;
                if (xx >= 0)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != queen.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == queen.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                xx++;
                if (xx <= 7)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != queen.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == queen.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                yy--;
                if (yy >= 0)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != queen.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == queen.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                yy++;
                if (yy <= 7)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != queen.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == queen.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                xx--;
                yy--;
                if (xx >= 0 && yy >= 0)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != queen.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == queen.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                xx++;
                yy++;
                if (xx <= 7 && yy <= 7)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != queen.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == queen.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                xx--;
                yy++;
                if (xx >= 0 && yy <= 7)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != queen.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == queen.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            xx = x;
            yy = y;
            while (true)
            {
                xx++;
                yy--;
                if (xx <= 7 && yy >= 0)
                {
                    if (map[yy, xx] == null)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                    }
                    else if (map[yy, xx].Player != queen.Player)
                    {
                        moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                        break;
                    }
                    else if (map[yy, xx].Player == queen.Player)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        private void CalculateKingMoves(Piece king, int x, int y, Piece[,] map)
        {
            List<Move> moves = king.PossibleMoves;
            moves.Clear();
            int xx = x;
            int yy = y;
            xx--;
            if (xx >= 0)
            {
                if (map[yy, xx] == null)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
                else if (map[yy, xx].Player != king.Player)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
            }
            xx = x;
            yy = y;
            xx++;
            if (xx <= 7)
            {
                if (map[yy, xx] == null)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
                else if (map[yy, xx].Player != king.Player)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
            }
            xx = x;
            yy = y;
            yy--;
            if (yy >= 0)
            {
                if (map[yy, xx] == null)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
                else if (map[yy, xx].Player != king.Player)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
            }
            xx = x;
            yy = y;
            yy++;
            if (yy <= 7)
            {
                if (map[yy, xx] == null)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
                else if (map[yy, xx].Player != king.Player)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
            }
            xx = x;
            yy = y;
            xx--;
            yy--;
            if (xx >= 0 && yy >= 0)
            {
                if (map[yy, xx] == null)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
                else if (map[yy, xx].Player != king.Player)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
            }
            xx = x;
            yy = y;
            xx++;
            yy++;
            if (xx <= 7 && yy <= 7)
            {
                if (map[yy, xx] == null)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
                else if (map[yy, xx].Player != king.Player)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
            }
            xx = x;
            yy = y;
            xx--;
            yy++;
            if (xx >= 0 && yy <= 7)
            {
                if (map[yy, xx] == null)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
                else if (map[yy, xx].Player != king.Player)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
            }
            xx = x;
            yy = y;
            xx++;
            yy--;
            if (xx <= 7 && yy >= 0)
            {
                if (map[yy, xx] == null)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
                else if (map[yy, xx].Player != king.Player)
                {
                    moves.Add(new Move(xx, yy, MoveType.Normal_Move));
                }
            }
        }
        private Piece[,] CopyMap(Piece[,] map)
        {
            Piece[,] newMap = new Piece[8, 8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Piece piece = map[i, j];
                    if (piece != null)
                    {
                        Piece copyPiece = new Piece(piece.Type, piece.Player);
                        newMap[i, j] = copyPiece;
                    }
                }
            }
            return newMap;
        }

        private int CountPossibleMoves(PlayerType player)
        {
            int cnt = 0;

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Piece piece = GetPiece(j, i);
                    if (piece != null && piece.Player == player)
                    {
                        cnt += piece.PossibleMoves.Count;
                    }
                }
            }

            return cnt;
        }

        public bool SimulateIfCheckWhenGoToThere(PlayerType player, int xFrom, int yFrom, int xTo, int yTo)
        {
            Piece[,] map = CopyMap(mMap);
            Piece piece = map[yFrom, xFrom];

            NormalMove(xFrom, yFrom, xTo, yTo, map);
            CalculateMoves(map);
            return ValidateIfCheck(map, player);
        }

        private bool SimulateIfCheckCanbeSolved(PlayerType player)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Piece piece = mMap[i, j];
                    if (piece != null && piece.Player == player)
                    {
                        List<Move> moves = piece.PossibleMoves;
                        foreach (Move move in moves)
                        {
                            int xTo = move.X;
                            int yTo = move.Y;
                            if (!SimulateIfCheckWhenGoToThere(player, j, i, xTo, yTo))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        private bool SimulateIfAllPossibleMovesMakeCheck(PlayerType player)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Piece piece = mMap[i, j];
                    if (piece != null && piece.Player == player)
                    {
                        List<Move> moves = piece.PossibleMoves;
                        foreach (Move move in moves)
                        {
                            int xTo = move.X;
                            int yTo = move.Y;
                            if (!SimulateIfCheckWhenGoToThere(player, j, i, xTo, yTo))
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}
