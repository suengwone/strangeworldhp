﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;

public class MultiImageTracker : MonoBehaviour
{
    // 이미지를 인식하면 해당 이미지에 대응하는 프리팹을 생성하고 싶다.
    // 이미지 인식 여부, 이미지의 라이브러리상 이름, 프리팹

    ARTrackedImageManager imageManager;

    Dictionary<string, GameObject> imagePrefab = new Dictionary<string, GameObject>();

    void Start()
    {
        imageManager = GetComponent<ARTrackedImageManager>();

        // 이미지를 인식했을 때 실행할 함수를 연결(binding)
        imageManager.trackedImagesChanged += ShowModel;
    }

    void ShowModel(ARTrackedImagesChangedEventArgs args)
    {

        // 인식한 이미지 리스트 전부를 순회한다.
        foreach (ARTrackedImage tracked in args.added)
        {
            // 인식한 이미지의 라이브러리 상의 이름을 가져온다.
            string imageName = tracked.referenceImage.name;
            

            // Resources 폴더에서 같은 이름으로 된 프리팹을 생성한다.
            GameObject go = Resources.Load<GameObject>(imageName);
            GameObject pref = Instantiate(go);
            imagePrefab.Add(imageName, pref);

            // 생성된 프리팹의 위치를 인식한 이미지의 위치와 동일하게 한다.
            pref.transform.position = tracked.transform.position;
        }
        

        // 인식 '중'인 이미지 리스트 전부를 순회한다
        //foreach (ARTrackedImage tracked in args.updated)
        //{
        //    string imageName = tracked.referenceImage.name;
        //    logs += "\r\n" + i++ + ": " + imageName;

        //    // 프리팹 이름으로 검색된 오브젝트의 위치를 이미지의 위치와 동기화시킨다.
        //    imagePrefab[imageName].transform.position = tracked.transform.position;
        //}
        //logText.text = logs;
    }

    void Update()
    {

    }
}
