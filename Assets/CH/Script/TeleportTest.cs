﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

public class TeleportTest : MonoBehaviour
{
    public Animator bearAnim;
    Camera camera;
    public Camera camera1;
    private void Awake()
    {
        camera = GameObject.Find("AR Camera").GetComponent<Camera>();
        camera1.gameObject.SetActive(false);
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("MainCamera"))
        {
            //camera1.gameObject.SetActive(true);
            //camera1.gameObject.tag = "MainCamera";
            //camera.gameObject.SetActive(false);
            //ARSessionOrigin arSO = GameObject.Find("AR Session Origin").GetComponent<ARSessionOrigin>();
            //arSO.camera = camera1;

            camera.cullingMask = -1;

            bearAnim.enabled = true;

        }
    }
}
