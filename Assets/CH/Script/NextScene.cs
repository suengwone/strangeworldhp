﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour
{
    public int sceneNum;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("MainCamera"))
        {
            SceneManager.LoadScene(sceneNum+1);

        }
    }
    public void ButtonDown()
    {
        SceneManager.LoadScene(1);
    }
    private void Start()
    {
        print(sceneNum);
    }
}
