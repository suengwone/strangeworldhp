﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 사용자의 마우스 입력에따라 상하좌우로 회전하고 싶다.
// 필요속성 : 회전속도, 현재 회전 값
public class CamRotate : MonoBehaviour
{
    // 필요속성 : 회전속도, 현재 회전 값
    public float rotSpeed = 200;
    Vector3 angle = Vector3.zero;
    float x;
    float y;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // 사용자의 마우스 입력에따라 상하좌우로 회전하고 싶다.
        // 1. 사용자 마우스 입력에따라
        float mx = Input.GetAxis("Mouse X");
        float my = Input.GetAxis("Mouse Y");
        /*
        x += mx * rotSpeed * Time.deltaTime;
        y += my * rotSpeed * Time.deltaTime;

        x = Mathf.Clamp(x, -60, 60);
        transform.eulerAngles = new Vector3(-y, x, 0);
        */

        // 2. 방향이필요 P = P0 +vt
        Vector3 dir = new Vector3(-my, mx, 0);
        // 3. 회전하고 싶다.
        angle += dir * rotSpeed * Time.deltaTime;
        // 각도를 유니티에 적용한다.
        // 각도를 -60 ~ +60 으로 제한한다.
        angle.x = Mathf.Clamp(angle.x, -60, 60);
        // 유니티 내부에서는 음수이면 +360 도를 더한다.
        transform.eulerAngles = angle;
    }
}
