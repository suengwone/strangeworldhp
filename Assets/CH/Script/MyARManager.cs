﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;

public class MyARManager : MonoBehaviour
{
    public GameObject indicator;
    public GameObject myCar;

    ARRaycastManager arRay;
    Vector2 screenCenter;
    GameObject myIndicator;
    GameObject sportCar;

    Color[] carColor = new Color[3] { Color.white, Color.red, Color.blue };

    void Start()
    {
        // AR용 레이캐스트 컴포넌트를 가져온다.
        arRay = GetComponent<ARRaycastManager>();

        // 화면의 정 중앙 픽셀을 지정한다.
        screenCenter = new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);

        myIndicator = Instantiate(indicator);
        myIndicator.SetActive(false);
        //myCar.SetActive(false);
        myCar.transform.localScale = Vector3.zero;

        Cursor.lockState = CursorLockMode.Locked;

    
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(bigger());
        }

        interActionButton();
        if (myCar != null)
        {
            myIndicator.SetActive(false);
            return;
        }
        DetectCenterPlane();

        // 인디케이터가 활성화된 상태에서 화면을 손가락으로 터치하면 자동차 오브젝트를 생성한다.
        // 손가락으로 터치하였는지 체크한다.
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            
            // 1. 인디케이터가 활성화 상태인지 체크한다.
            // 2. UI 오브젝트를 터치하지 않은 상태
            if (myIndicator.activeSelf && !EventSystem.current.currentSelectedGameObject)
            {
                myCar.transform.position = myIndicator.transform.position;
                myCar.transform.rotation = myIndicator.transform.rotation;
                StartCoroutine(bigger());
            }
        }    
    }
    IEnumerator bigger()
    {
        while (true)
        {
            if (myCar.transform.localScale.x > 0.95f)
            {
                break;
            }
            myCar.transform.localScale = Vector3.Lerp(myCar.transform.localScale, Vector3.one, 0.2f * Time.deltaTime);
            yield return null;
        }
    }

    void interActionButton() 
    {
        if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
            //Touch touch = Input.GetTouch(0);
            Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
            RaycastHit hitInfo;
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward, Color.red, 20f);
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << LayerMask.NameToLayer("Button")))
            {
                ButtonEvent.Ontouch();
            }
        }
    }


    void DetectCenterPlane()
    {
        // 내가 스마트폰의 카메라를 비추면 그 화면의 정 중앙 부분이 AR 카메라가 인식한 바닥인지 체크한다.
        List<ARRaycastHit> hitInfos = new List<ARRaycastHit>();
        if (arRay.Raycast(screenCenter, hitInfos, TrackableType.Planes))
        {
            // 만일 바닥이라면, 인디케이터를 바닥면에 표시한다.
            // 위치 마크 오브젝트를 카메라 중앙의 위치로 맞춰놓고 활성화한다.
            myIndicator.transform.position = hitInfos[0].pose.position;
            Vector3 dir = Camera.main.transform.forward;
            dir.y = 0;
            Quaternion camRot = Quaternion.LookRotation(dir);
            myIndicator.transform.rotation = camRot;
            myIndicator.SetActive(true);
        }
        else
        {
            myIndicator.SetActive(false);
        }
    }

    // 자동차의 색상을 버튼에 할당된 색상으로 변경한다.
    public void ChangeColor(int colorIdx)
    {
        if(sportCar != null)
        {
            sportCar.transform.Find("body").GetComponent<MeshRenderer>().material.color = carColor[colorIdx];
        }
    }


}
