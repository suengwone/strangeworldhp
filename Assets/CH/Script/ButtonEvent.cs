﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent : MonoBehaviour
{
    public delegate void touchButton();
    public static touchButton Ontouch; 
    void Start()
    {
        Ontouch = ButtonDown;
    }

    void ButtonDown()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }
}
