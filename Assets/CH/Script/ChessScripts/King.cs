﻿using System.Collections;
using UnityEngine;

public class King : Chessman
{
    public override bool[,] PossibleMoves()
    {
        bool[,] r = new bool[8, 8];

        Move(CurrentX + 1, CurrentY, ref r); // up
        Move(CurrentX - 1, CurrentY, ref r); // down
        Move(CurrentX, CurrentY - 1, ref r); // left
        Move(CurrentX, CurrentY + 1, ref r); // right
        Move(CurrentX + 1, CurrentY - 1, ref r); // up left
        Move(CurrentX - 1, CurrentY - 1, ref r); // down left
        Move(CurrentX + 1, CurrentY + 1, ref r); // up right
        Move(CurrentX - 1, CurrentY + 1, ref r); // down right

        return r;
    }

    public int currentchY;
    public int currentchX;
    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.name)
        {
            case "A":
                currentchY = 0;
                break;
            case "B":
                currentchY = 1;
                break;
            case "C":
                currentchY = 2;
                break;
            case "D":
                currentchY = 3;
                break;
            case "E":
                currentchY = 4;
                break;
            case "F":
                currentchY = 5;
                break;
            case "G":
                currentchY = 6;
                break;
            case "H":
                currentchY = 7;
                break;
        }
        switch (other.transform.parent.gameObject.name)
        {
            case "8":
                currentchX = 0;
                break;
            case "7":
                currentchX = 1;
                break;
            case "6":
                currentchX = 2;
                break;
            case "5":
                currentchX = 3;
                break;
            case "4":
                currentchX = 4;
                break;
            case "3":
                currentchX = 5;
                break;
            case "2":
                currentchX = 6;
                break;
            case "1":
                currentchX = 7;
                break;
        }
    }

}
