﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BoardHighlights : MonoBehaviour
{

    public static BoardHighlights Instance { set; get; }

    public GameObject highlightPrefab;
    private List<GameObject> highlights;

    private void Start()
    {
        Instance = this;
        highlights = new List<GameObject>();
    }

    private GameObject GetHighLightObject()
    {
        GameObject go = highlights.Find(g => !g.activeSelf);

        if (go == null)
        {
            go = Instantiate(highlightPrefab);
            highlights.Add(go);
        }

        return go;
    }

    public void HighLightAllowedMoves(bool[,] moves)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (moves[i, j])
                {
                    GameObject go = GetHighLightObject();
                    go.SetActive(true);
                    go.transform.position = new Vector3((i * 1.6f) + 0.8f, 0.0001f, (j * 1.6f) + 0.8f) +
                        GameObject.Find("testChessOrigin").transform.position;
                    string temp = string.Empty;
                    switch (j)
                    {
                        case 0:
                            temp += "H";
                            break;
                        case 1:
                            temp += "G";
                            break;
                        case 2:
                            temp += "F";
                            break;
                        case 3:
                            temp += "E";
                            break;
                        case 4:
                            temp += "D";
                            break;
                        case 5:
                            temp += "C";
                            break;
                        case 6:
                            temp += "B";
                            break;
                        case 7:
                            temp += "A";
                            break;
                    }
                    switch (i)
                    {
                        case 0:
                            temp += "1";
                            break;
                        case 1:
                            temp += "2";
                            break;
                        case 2:
                            temp += "3";
                            break;
                        case 3:
                            temp += "4";
                            break;
                        case 4:
                            temp += "5";
                            break;
                        case 5:
                            temp += "6";
                            break;
                        case 6:
                            temp += "7";
                            break;
                        case 7:
                            temp += "8";
                            break;
                    }
                    go.transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>().text = temp;

                }
            }

        }
    }

    public void HideHighlights()
    {
        foreach (GameObject go in highlights)
            go.SetActive(false);
    }
}
