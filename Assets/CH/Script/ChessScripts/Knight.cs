﻿using System.Collections;
using UnityEngine;

public class Knight : Chessman
{
    public override bool[,] PossibleMoves()
    {
        bool[,] r = new bool[8, 8];

        // Up left
        Move(CurrentX - 1, CurrentY + 2, ref r);

        // Up right
        Move(CurrentX + 1, CurrentY + 2, ref r);

        // Down left
        Move(CurrentX - 1, CurrentY - 2, ref r);

        // Down right
        Move(CurrentX + 1, CurrentY - 2, ref r);


        // Left Down
        Move(CurrentX - 2, CurrentY - 1, ref r);

        // Right Down
        Move(CurrentX + 2, CurrentY - 1, ref r);

        // Left Up
        Move(CurrentX - 2, CurrentY + 1, ref r);

        // Right Up
        Move(CurrentX + 2, CurrentY + 1, ref r);

        return r;
    }
    public int currentchY;
    public int currentchX;
    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.name)
        {
            case "A":
                currentchY = 0;
                break;
            case "B":
                currentchY = 1;
                break;
            case "C":
                currentchY = 2;
                break;
            case "D":
                currentchY = 3;
                break;
            case "E":
                currentchY = 4;
                break;
            case "F":
                currentchY = 5;
                break;
            case "G":
                currentchY = 6;
                break;
            case "H":
                currentchY = 7;
                break;
        }
        switch (other.transform.parent.gameObject.name)
        {
            case "8":
                currentchX = 0;
                break;
            case "7":
                currentchX = 1;
                break;
            case "6":
                currentchX = 2;
                break;
            case "5":
                currentchX = 3;
                break;
            case "4":
                currentchX = 4;
                break;
            case "3":
                currentchX = 5;
                break;
            case "2":
                currentchX = 6;
                break;
            case "1":
                currentchX = 7;
                break;
        }
        if (other.gameObject.tag == "King")
        {
            Loot_Drop_FX.instance.lookActiveStone();
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Black")
        {
            Destroy(other.gameObject);
        }
    }
}
