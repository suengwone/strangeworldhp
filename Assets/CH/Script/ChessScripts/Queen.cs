﻿using System.Collections;
using UnityEngine;

public class Queen : Chessman
{

    public override bool[,] PossibleMoves()
    {
        bool[,] r = new bool[8, 8];

        int i, j;

        // Top left
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i--;
            j++;
            if (i < 0 || j >= 8) break;

            if (Move(i, j, ref r)) break;
        }

        // Top right
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i++;
            j++;
            if (i >= 8 || j >= 8) break;

            if (Move(i, j, ref r)) break;
        }

        // Down left
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i--;
            j--;
            if (i < 0 || j < 0) break;

            if (Move(i, j, ref r)) break;
        }

        // Down right
        i = CurrentX;
        j = CurrentY;
        while (true)
        {
            i++;
            j--;
            if (i >= 8 || j < 0) break;

            if (Move(i, j, ref r)) break;
        }

        // Right
        i = CurrentX;
        while (true)
        {
            i++;
            if (i >= 8) break;

            if (Move(i, CurrentY, ref r)) break;
        }

        // Left
        i = CurrentX;
        while (true)
        {
            i--;
            if (i < 0) break;

            if (Move(i, CurrentY, ref r)) break;
        }

        // Up
        i = CurrentY;
        while (true)
        {
            i++;
            if (i >= 8) break;

            if (Move(CurrentX, i, ref r)) break;
        }

        // Down
        i = CurrentY;
        while (true)
        {
            i--;
            if (i < 0) break;

            if (Move(CurrentX, i, ref r)) break;

        }

        return r;
    }

    public int currentchY;
    public int currentchX;
    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.name)
        {
            case "A":
                currentchY = 0;
                break;
            case "B":
                currentchY = 1;
                break;
            case "C":
                currentchY = 2;
                break;
            case "D":
                currentchY = 3;
                break;
            case "E":
                currentchY = 4;
                break;
            case "F":
                currentchY = 5;
                break;
            case "G":
                currentchY = 6;
                break;
            case "H":
                currentchY = 7;
                break;
        }
        switch (other.transform.parent.gameObject.name)
        {
            case "8":
                currentchX = 0;
                break;
            case "7":
                currentchX = 1;
                break;
            case "6":
                currentchX = 2;
                break;
            case "5":
                currentchX = 3;
                break;
            case "4":
                currentchX = 4;
                break;
            case "3":
                currentchX = 5;
                break;
            case "2":
                currentchX = 6;
                break;
            case "1":
                currentchX = 7;
                break;
        }
        if (other.gameObject.tag == "King")
        {
            Loot_Drop_FX.instance.lookActiveStone();
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Black")
        {
            Destroy(other.gameObject);
        }
    }
}
