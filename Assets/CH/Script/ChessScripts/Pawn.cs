﻿using System.Collections;
using UnityEngine;

public class Pawn : Chessman
{

    public override bool[,] PossibleMoves()
    {
        bool[,] r = new bool[8, 8];

        Chessman c, c2;

        int[] e = ChessSpeechManager.instance.EnPassantMove;

        //if (isWhite)
        //{
        ////// White team move //////

        // Diagonal left
        if (CurrentX != 0 && CurrentY != 7)
        {
            if (e[0] == CurrentX - 1 && e[1] == CurrentY + 1)
                r[CurrentX - 1, CurrentY + 1] = true;

            c = BoardManager.Instance.Chessmans[CurrentX - 1, CurrentY + 1];
            if (c != null && !c.isWhite)
                r[CurrentX - 1, CurrentY + 1] = true;
        }

        // Diagonal right
        if (CurrentX != 7 && CurrentY != 7)
        {
            if (e[0] == CurrentX + 1 && e[1] == CurrentY + 1)
                r[CurrentX + 1, CurrentY + 1] = true;

            c = BoardManager.Instance.Chessmans[CurrentX + 1, CurrentY + 1];
            if (c != null && !c.isWhite)
                r[CurrentX + 1, CurrentY + 1] = true;
        }

        // Middle
        if (CurrentY != 7)
        {
            c = BoardManager.Instance.Chessmans[CurrentX, CurrentY + 1];
            if (c == null)
                r[CurrentX, CurrentY + 1] = true;
        }

        // Middle on first move
        if (CurrentY == 1)
        {
            c = BoardManager.Instance.Chessmans[CurrentX, CurrentY + 1];
            c2 = BoardManager.Instance.Chessmans[CurrentX, CurrentY + 2];
            if (c == null && c2 == null)
                r[CurrentX, CurrentY + 2] = true;
        }
        //}
        //else
        //{
        //    ////// Black team move //////

        //    // Diagonal left
        //    if (CurrentX != 0 && CurrentY != 0)
        //    {
        //        if (e[0] == CurrentX - 1 && e[1] == CurrentY - 1)
        //            r[CurrentX - 1, CurrentY - 1] = true;

        //        c = BoardManager.Instance.Chessmans[CurrentX - 1, CurrentY - 1];
        //        if (c != null && c.isWhite)
        //            r[CurrentX - 1, CurrentY - 1] = true;
        //    }

        //    // Diagonal right
        //    if (CurrentX != 7 && CurrentY != 0)
        //    {
        //        if (e[0] == CurrentX + 1 && e[1] == CurrentY - 1)
        //            r[CurrentX + 1, CurrentY - 1] = true;

        //        c = BoardManager.Instance.Chessmans[CurrentX + 1, CurrentY - 1];
        //        if (c != null && c.isWhite)
        //            r[CurrentX + 1, CurrentY - 1] = true;
        //    }

        //    // Middle
        //    if (CurrentY != 0)
        //    {
        //        c = BoardManager.Instance.Chessmans[CurrentX, CurrentY - 1];
        //        if (c == null)
        //            r[CurrentX, CurrentY - 1] = true;
        //    }

        //    // Middle on first move
        //    if (CurrentY == 6)
        //    {
        //        c = BoardManager.Instance.Chessmans[CurrentX, CurrentY - 1];
        //        c2 = BoardManager.Instance.Chessmans[CurrentX, CurrentY - 2];
        //        if (c == null && c2 == null)
        //            r[CurrentX, CurrentY - 2] = true;
        //    }
        //}

        return r;
    }

    private void Update()
    {
        //print(currentchX + "'" + currentchY);
    }
    public int currentchY;
    public int currentchX;
    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.name)
        {
            case "A":
                currentchY = 0;
                break;
            case "B":
                currentchY = 1;
                break;
            case "C":
                currentchY = 2;
                break;
            case "D":
                currentchY = 3;
                break;
            case "E":
                currentchY = 4;
                break;
            case "F":
                currentchY = 5;
                break;
            case "G":
                currentchY = 6;
                break;
            case "H":
                currentchY = 7;
                break;
        }
        switch (other.transform.parent.gameObject.name)
        {
            case "8":
                currentchX = 0;
                break;
            case "7":
                currentchX = 1;
                break;
            case "6":
                currentchX = 2;
                break;
            case "5":
                currentchX = 3;
                break;
            case "4":
                currentchX = 4;
                break;
            case "3":
                currentchX = 5;
                break;
            case "2":
                currentchX = 6;
                break;
            case "1":
                currentchX = 7;
                break;
        }
        if (other.gameObject.tag == "King")
        {
            Loot_Drop_FX.instance.lookActiveStone();
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Black")
        {
            Destroy(other.gameObject);
        }

    }
}
