﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessPiece : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "King")
        {
            Loot_Drop_FX.instance.lookActiveStone();
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Black")
        {
            Destroy(other.gameObject);
        }
    }
}
