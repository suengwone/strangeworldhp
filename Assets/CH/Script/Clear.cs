﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RayFire;
public class Clear : MonoBehaviour
{
    List<GameObject> rfr;
    public List<GameObject> fire;
    GameObject plane;
    GameObject map;
    GameObject chessObj;
    GameObject lightning;
    GameObject cube;
    bool onClear;
    void Start()
    {
        cube = GameObject.Find("Loot_Cube_Red");
        map = GameObject.Find("Center_Hall");
        rfr = new List<GameObject>();
        chessObj = GameObject.Find("ChessObject");
        lightning = GameObject.Find("Lighting");
        plane = GameObject.Find("Plane");
        for (int i = 0; i < map.transform.childCount; i++)
        {
            rfr.Add(map.transform.GetChild(i).gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

        //if (Input.touchCount > 0)
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    foreach (var temp in rfr)
        //    {
        //        temp.GetComponent<RayfireRigid>().Initialize();
        //    }
        //    chessObj.SetActive(false);
        //    lightning.SetActive(false);
        //    onClear = true;
        //    //Touch touch = Input.GetTouch(0);
        //    //Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        //    //RaycastHit hitInfo;

        //    //if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << LayerMask.NameToLayer("Button")))
        //    //{
        //    //}
        //    //else
        //    //{
        //    //    cube.get;
        //    //}
        //}
        if (onClear)
        {
            plane.transform.position += Vector3.down * 5f * Time.deltaTime;
        }
    }


    public void GameClear()
    {
        foreach (var temp in rfr)
        {
            temp.GetComponent<RayfireRigid>().Initialize();
        }
        foreach (var item in fire)
        {
            item.SetActive(false);
        }
        chessObj.SetActive(false);
        lightning.SetActive(false);
        onClear = true;
    }
}
