﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessBoard : MonoBehaviour
{
    public static ChessBoard instance;
    public GameObject oneLine;
    public static Transform[] ChessOneLine = new Transform[9];
    public GameObject twoLine;
    public static Transform[] ChessTwoLine = new Transform[9];
    public GameObject threeLine;
    public static Transform[] ChessThreeLine = new Transform[9];
    public GameObject fourLine;
    public static Transform[] ChessFourLine = new Transform[9];
    public GameObject fiveLine;
    public static Transform[] ChessFiveLine = new Transform[9];
    public GameObject sixLine;
    public static Transform[] ChessSixLine = new Transform[9];
    public GameObject sevenLine;
    public static Transform[] ChessSevenLine = new Transform[9];
    public GameObject eightLine;
    public static Transform[] ChessEightLine = new Transform[9];

    Vector3 bojung = new Vector3(0, 0.74f, 0);
    public static Dictionary<string, Vector3> dicChess;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        for (int i = 1; i < ChessOneLine.Length; i++)
        {
            ChessOneLine[i] = oneLine.transform.GetChild(8 - i);
            ChessOneLine[i].GetComponent<BoxCollider>().center = new Vector3(0, 1f, 0);
            ChessOneLine[i].GetComponent<BoxCollider>().isTrigger = true;
        }
        for (int i = 1; i < ChessTwoLine.Length; i++)
        {
            ChessTwoLine[i] = twoLine.transform.GetChild(8 - i);
            ChessTwoLine[i].GetComponent<BoxCollider>().center = new Vector3(0, 1f, 0);
            ChessTwoLine[i].GetComponent<BoxCollider>().isTrigger = true;
        }
        for (int i = 1; i < ChessThreeLine.Length; i++)
        {
            ChessThreeLine[i] = threeLine.transform.GetChild(8 - i);
            ChessThreeLine[i].GetComponent<BoxCollider>().center = new Vector3(0, 1f, 0);
            ChessThreeLine[i].GetComponent<BoxCollider>().isTrigger = true;
        }
        for (int i = 1; i < ChessFourLine.Length; i++)
        {
            ChessFourLine[i] = fourLine.transform.GetChild(8 - i);
            ChessFourLine[i].GetComponent<BoxCollider>().center = new Vector3(0, 1f, 0);
            ChessFourLine[i].GetComponent<BoxCollider>().isTrigger = true;
        }
        for (int i = 1; i < ChessFiveLine.Length; i++)
        {
            ChessFiveLine[i] = fiveLine.transform.GetChild(8 - i);
            ChessFiveLine[i].GetComponent<BoxCollider>().center = new Vector3(0, 1f, 0);
            ChessFiveLine[i].GetComponent<BoxCollider>().isTrigger = true;
        }
        for (int i = 1; i < ChessSixLine.Length; i++)
        {
            ChessSixLine[i] = sixLine.transform.GetChild(8 - i);
            ChessSixLine[i].GetComponent<BoxCollider>().center = new Vector3(0, 1f, 0);
            ChessSixLine[i].GetComponent<BoxCollider>().isTrigger = true;
        }
        for (int i = 1; i < ChessSevenLine.Length; i++)
        {
            ChessSevenLine[i] = sevenLine.transform.GetChild(8 - i);
            ChessSevenLine[i].GetComponent<BoxCollider>().center = new Vector3(0, 1f, 0);
            ChessSevenLine[i].GetComponent<BoxCollider>().isTrigger = true;
        }
        for (int i = 1; i < ChessEightLine.Length; i++)
        {
            ChessEightLine[i] = eightLine.transform.GetChild(8 - i);
            ChessEightLine[i].GetComponent<BoxCollider>().center = new Vector3(0, 1f, 0);
            ChessEightLine[i].GetComponent<BoxCollider>().isTrigger = true;
        }


        dicChess = new Dictionary<string, Vector3>();
        //dicChess.Add("A1.", ChessOneLine[1].position + bojung);
        //dicChess.Add("A2.", ChessTwoLine[1].position + bojung);
        //dicChess.Add("A3.", ChessThreeLine[1].position + bojung);
        //dicChess.Add("A4.", ChessFourLine[1].position + bojung);
        //dicChess.Add("A5.", ChessFiveLine[1].position + bojung);
        //dicChess.Add("A6.", ChessSixLine[1].position + bojung);
        //dicChess.Add("A7.", ChessSevenLine[1].position + bojung);
        //dicChess.Add("A8.", ChessEightLine[1].position + bojung);

        //dicChess.Add("B1.", ChessOneLine[2].position + bojung);
        //dicChess.Add("B2.", ChessTwoLine[2].position + bojung);
        //dicChess.Add("B3.", ChessThreeLine[2].position + bojung);
        //dicChess.Add("B4.", ChessFourLine[2].position + bojung);
        //dicChess.Add("b5.", ChessFiveLine[2].position + bojung);
        //dicChess.Add("B6.", ChessSixLine[2].position + bojung);
        //dicChess.Add("B7.", ChessSevenLine[2].position + bojung);
        //dicChess.Add("B8.", ChessEightLine[2].position + bojung);

        //dicChess.Add("C1.", ChessOneLine[3].position + bojung);
        //dicChess.Add("C2.", ChessTwoLine[3].position + bojung);
        //dicChess.Add("C3.", ChessThreeLine[3].position + bojung);
        //dicChess.Add("C4.", ChessFourLine[3].position + bojung);
        //dicChess.Add("C5.", ChessFiveLine[3].position + bojung);
        //dicChess.Add("C6.", ChessSixLine[3].position + bojung);
        //dicChess.Add("C7.", ChessSevenLine[3].position + bojung);
        //dicChess.Add("C8.", ChessEightLine[3].position + bojung);

        //dicChess.Add("D1.", ChessOneLine[4].position + bojung);
        //dicChess.Add("D2.", ChessTwoLine[4].position + bojung);
        //dicChess.Add("D3.", ChessThreeLine[4].position + bojung);
        //dicChess.Add("D4.", ChessFourLine[4].position + bojung);
        //dicChess.Add("D5.", ChessFiveLine[4].position + bojung);
        //dicChess.Add("D6.", ChessSixLine[4].position + bojung);
        //dicChess.Add("D7.", ChessSevenLine[4].position + bojung);
        //dicChess.Add("D8.", ChessEightLine[4].position + bojung);

        //dicChess.Add("E1.", ChessOneLine[5].position + bojung);
        //dicChess.Add("E2.", ChessTwoLine[5].position + bojung);
        //dicChess.Add("E3.", ChessThreeLine[5].position + bojung);
        //dicChess.Add("E4.", ChessFourLine[5].position + bojung);
        //dicChess.Add("E5.", ChessFiveLine[5].position + bojung);
        //dicChess.Add("E6.", ChessSixLine[5].position + bojung);
        //dicChess.Add("E7.", ChessSevenLine[5].position + bojung);
        //dicChess.Add("E8.", ChessEightLine[5].position + bojung);

        //dicChess.Add("F1.", ChessOneLine[6].position + bojung);
        //dicChess.Add("F2.", ChessTwoLine[6].position + bojung);
        //dicChess.Add("F3.", ChessThreeLine[6].position + bojung);
        //dicChess.Add("F4.", ChessFourLine[6].position + bojung);
        //dicChess.Add("F5.", ChessFiveLine[6].position + bojung);
        //dicChess.Add("F6.", ChessSixLine[6].position + bojung);
        //dicChess.Add("F7.", ChessSevenLine[6].position + bojung);
        //dicChess.Add("F8.", ChessEightLine[6].position + bojung);

        //dicChess.Add("G1.", ChessOneLine[7].position + bojung);
        //dicChess.Add("G2.", ChessTwoLine[7].position + bojung);
        //dicChess.Add("G3.", ChessThreeLine[7].position + bojung);
        //dicChess.Add("G4.", ChessFourLine[7].position + bojung);
        //dicChess.Add("G5.", ChessFiveLine[7].position + bojung);
        //dicChess.Add("G6.", ChessSixLine[7].position + bojung);
        //dicChess.Add("G7.", ChessSevenLine[7].position + bojung);
        //dicChess.Add("G8.", ChessEightLine[7].position + bojung);

        //dicChess.Add("H1.", ChessOneLine[8].position + bojung);
        //dicChess.Add("H2.", ChessTwoLine[8].position + bojung);
        //dicChess.Add("H3.", ChessThreeLine[8].position + bojung);
        //dicChess.Add("H4.", ChessFourLine[8].position + bojung);
        //dicChess.Add("H5.", ChessFiveLine[8].position + bojung);
        //dicChess.Add("H6.", ChessSixLine[8].position + bojung);
        //dicChess.Add("H7.", ChessSevenLine[8].position + bojung);
        //dicChess.Add("H8.", ChessEightLine[8].position + bojung);

    }
    //public void changeSW(int x, int y)
    //{
    //}
    private void Update()
    {
        //foreach(KeyValuePair<string, Vector3> pair in dicChess)
        //{
        //    string temp = pair.Key;
        //    Vector3 temp1 = pair.Value;
        //    print(temp);
        //    print(temp1);
        //}
    }

    public void DictionaryAdd(int x, int y)
    {
        string temp = string.Empty;
        int tempNum =0;
        Vector3 dest = Vector3.zero;
        switch (y)
        {
            case 0:
                temp += "H";
                tempNum = 8;
                break;
            case 1:
                temp += "G";
                tempNum = 7;
                break;
            case 2:
                temp += "F";
                tempNum = 6;
                break;
            case 3:
                temp += "E";
                tempNum = 5;
                break;
            case 4:
                temp += "D";
                tempNum = 4;
                break;
            case 5:
                temp += "C";
                tempNum = 3;
                break;
            case 6:
                temp += "B";
                tempNum = 2;
                break;
            case 7:
                temp += "A";
                tempNum = 1;
                break;
        }
        switch (x)
        {
            case 0:
                temp += "1";
                dest = ChessOneLine[tempNum].position;
                break;
            case 1:
                temp += "2";
                dest = ChessTwoLine[tempNum].position;
                break;
            case 2:
                temp += "3";
                dest = ChessThreeLine[tempNum].position;
                break;
            case 3:
                temp += "4";
                dest = ChessFourLine[tempNum].position;
                break;
            case 4:
                temp += "5";
                dest = ChessFiveLine[tempNum].position;
                break;
            case 5:
                temp += "6";
                dest = ChessSixLine[tempNum].position;
                break;
            case 6:
                temp += "7";
                dest = ChessSevenLine[tempNum].position;
                break;
            case 7:
                temp += "8";
                dest = ChessEightLine[tempNum].position;
                break;
        }
        dicChess.Add(temp + ".", dest + bojung);
    }
}
