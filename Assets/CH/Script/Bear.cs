﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bear : MonoBehaviour
{
    Animator anim;
    public AudioSource audio;
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    bool awake = true;
    // Update is called once per frame
    void Update()
    {
        if (Switch.onPlay && awake)
        {
            anim.SetTrigger("ToSleep");
            awake = false;
        }
    }
    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireSphere(transform.position, 6f);
    //}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "MainCamera")
        {
            anim.SetTrigger("Attack");
        }
    }

    void PlaySound()
    {
        audio.Play();
    }

}
