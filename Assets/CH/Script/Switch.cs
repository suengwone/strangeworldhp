﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public AudioSource audio;
    public static bool onPlay;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Button"))
        {
            audio.Play();
            StartCoroutine(sleepbear());
        }
    }

    IEnumerator sleepbear()
    {
        yield return new WaitForSeconds(5f);
        onPlay = true;
    }
}
