﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HighlightingSystem;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples;
public class ChessSpeechManager : MonoBehaviour
{
    public static ChessSpeechManager instance;
    public GameObject pawn;
    public GameObject queen;
    //public GameObject bishop;
    public GameObject king;
    //public GameObject Knight;
    //public GameObject rook;
    public GameObject[] knight = new GameObject[2];
    public GameObject[] bishop = new GameObject[2];
    public GameObject[] rook = new GameObject[2];
    GameObject selectedObj;
    bool onSelect = true;


    // 체스 함수 가지고 오기
    private bool[,] allowedMoves { get; set; }
    private List<GameObject> activeChessman;
    private const float TILE_SIZE = 1.0f;
    private const float TILE_OFFSET = 0.5f;

    public Chessman[,] Chessmans { get; set; }
    private Chessman selectedChessman;

    public int[] EnPassantMove { set; get; }
    int destX;
    int destY;

    Animator anim;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {

        EnPassantMove = new int[2] { -1, -1 };
        selectedObj = pawn;
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    selectedObj.GetComponent<Highlighter>().constant = false;
        //    pawn.GetComponent<Highlighter>().constant = true;
        //    selectedObj = pawn;
        //    BoardManager.Instance.SelectChessman(pawn.GetComponent<Knight>().currentchX, pawn.GetComponent<Knight>().currentchY);
        //    onSelect = true;
        //}
        //if (Input.GetKeyDown(KeyCode.F))
        //{
        //    selectedObj.GetComponent<Highlighter>().constant = false;
        //    pawn.GetComponent<Highlighter>().constant = true;
        //    selectedObj = pawn;
        //    BoardManager.Instance.SelectChessman(pawn.GetComponent<Pawn>().currentchX, pawn.GetComponent<Pawn>().currentchY);
        //    onSelect = true;
        //}
    }
    public void ChessSelect()
    {
        bool hasAtLeastOneMove = false;
        // 말을 선택하고 싶다.
        if (GCSR_Example.word.Contains("퀸"))
        {
            if (selectedObj.GetComponent<Highlighter>())
            {
                selectedObj.GetComponent<Highlighter>().constant = false;
            }
            else
            {
                selectedObj.transform.GetChild(0).GetComponent<Highlighter>().constant = false;
            }
            queen.transform.GetChild(0).GetComponent<Highlighter>().constant = true;
            selectedObj = queen;
            BoardManager.Instance.SelectChessman(selectedObj.GetComponent<Queen>().currentchX, queen.GetComponent<Queen>().currentchY);
        }
        else if (GCSR_Example.word.Contains("킹"))
        {
            if (selectedObj.GetComponent<Highlighter>())
            {
                selectedObj.GetComponent<Highlighter>().constant = false;
            }
            else
            {
                selectedObj.transform.GetChild(0).GetComponent<Highlighter>().constant = false;
            }
            king.transform.GetChild(0).GetComponent<Highlighter>().constant = true;
            selectedObj = king;
            BoardManager.Instance.SelectChessman(selectedObj.GetComponent<King>().currentchX, king.GetComponent<King>().currentchY);
        }
        else if (GCSR_Example.word.Contains("폰"))
        {
            selectedObj.GetComponent<Highlighter>().constant = false;
            pawn.GetComponent<Highlighter>().constant = true;
            selectedObj = pawn;
            BoardManager.Instance.SelectChessman(selectedObj.GetComponent<Pawn>().currentchX, pawn.GetComponent<Pawn>().currentchY);
        }
        else if (GCSR_Example.word.Contains("나이트1") || GCSR_Example.word.Contains("나이트일"))
        {
            selectedObj.GetComponent<Highlighter>().constant = false;
            knight[0].GetComponent<Highlighter>().constant = true;
            selectedObj = knight[0];
            BoardManager.Instance.SelectChessman(selectedObj.GetComponent<Knight>().currentchX, knight[0].GetComponent<Knight>().currentchY);
        }
        else if (GCSR_Example.word.Contains("나이트2") || GCSR_Example.word.Contains("나이트이"))
        {
            selectedObj.GetComponent<Highlighter>().constant = false;
            knight[1].GetComponent<Highlighter>().constant = true;
            selectedObj = knight[1];
            BoardManager.Instance.SelectChessman(selectedObj.GetComponent<Knight>().currentchX, knight[1].GetComponent<Knight>().currentchY);
        }
        else if (GCSR_Example.word.Contains("비숍1") || GCSR_Example.word.Contains("비숍일"))
        {
            selectedObj.GetComponent<Highlighter>().constant = false;
            bishop[0].GetComponent<Highlighter>().constant = true;
            selectedObj = bishop[0];
            BoardManager.Instance.SelectChessman(selectedObj.GetComponent<Bishop>().currentchX, bishop[0].GetComponent<Bishop>().currentchY);
        }
        else if (GCSR_Example.word.Contains("비숍2") || GCSR_Example.word.Contains("비숍이") || GCSR_Example.word.Contains("비쇼비"))
        {
            selectedObj.GetComponent<Highlighter>().constant = false;
            bishop[1].GetComponent<Highlighter>().constant = true;
            selectedObj = bishop[1];
            BoardManager.Instance.SelectChessman(selectedObj.GetComponent<Bishop>().currentchX, bishop[1].GetComponent<Bishop>().currentchY);
        }
        else if (GCSR_Example.word.Contains("룩1") || GCSR_Example.word.Contains("룩일"))
        {
            if (selectedObj.GetComponent<Highlighter>())
            {
                selectedObj.GetComponent<Highlighter>().constant = false;
            }
            else
            {
                selectedObj.transform.GetChild(0).GetComponent<Highlighter>().constant = false;
            }
            rook[0].transform.GetChild(0).GetComponent<Highlighter>().constant = true;
            selectedObj = rook[0];
            BoardManager.Instance.SelectChessman(selectedObj.GetComponent<Rook>().currentchX, rook[0].GetComponent<Rook>().currentchY);
        }
        else if (GCSR_Example.word.Contains("루기") || GCSR_Example.word.Contains("룩!"))
        {
            if (selectedObj.GetComponent<Highlighter>())
            {
                selectedObj.GetComponent<Highlighter>().constant = false;
            }
            else
            {
                selectedObj.transform.GetChild(0).GetComponent<Highlighter>().constant = false;
            }
            rook[1].transform.GetChild(0).GetComponent<Highlighter>().constant = true;
            selectedObj = rook[1];
            BoardManager.Instance.SelectChessman(selectedObj.GetComponent<Rook>().currentchX, rook[1].GetComponent<Rook>().currentchY);
        }
    }
    Vector3 bojung = new Vector3(0, 0.5f, 0);
    public void ChessOrder()
    {
        if (selectedObj != null)
        {
            print(GCSR_Example.word);
            char[] temp = new char[GCSR_Example.word.ToCharArray().Length];
            temp = GCSR_Example.word.ToCharArray();
            switch (temp[0])
            {
                case 'H':
                    destY = 0;
                    break;
                case 'G':
                    destY = 1;
                    break;
                case 'F':
                    destY = 2;
                    break;
                case 'E':
                    destY = 3;
                    break;
                case 'D':
                    destY = 4;
                    break;
                case 'C':
                    destY = 5;
                    break;
                case 'B':
                    destY = 6;
                    break;
                case 'A':
                    destY = 7;
                    break;
                default:
                    break;
            }
            switch (temp[1])
            {
                case '1':
                    destX = 0;
                    break;
                case '2':
                    destX = 1;
                    break;
                case '3':
                    destX = 2;
                    break;
                case '4':
                    destX = 3;
                    break;
                case '5':
                    destX = 4;
                    break;
                case '6':
                    destX = 5;
                    break;
                case '7':
                    destX = 6;
                    break;
                case '8':
                    destX = 7;
                    break;
                default:
                    break;
            }

            string keyCheck = string.Empty;
            foreach (KeyValuePair<string, Vector3> pair in ChessBoard.dicChess)
            {
                keyCheck += pair.Key.ToString();
            }

            //if (ChessBoard.dicChess.ContainsKey(GCSR_Example.word))
            if (keyCheck.Contains(GCSR_Example.word))
            {
                BoardHighlights.Instance.HideHighlights();
                BoardManager.Instance.Chessmans[BoardManager.Instance.selectedChessman.CurrentX, BoardManager.Instance.selectedChessman.CurrentY] = null;
                BoardManager.Instance.selectedChessman.SetPosition(destX, destY);
                BoardManager.Instance.Chessmans[destX, destY] = BoardManager.Instance.selectedChessman;
                StartCoroutine(MoveChessObj(ChessBoard.dicChess[GCSR_Example.word]));
            }
        }
    }

    IEnumerator MoveChessObj(Vector3 dest)
    {
        Vector3 dir = dest - selectedObj.transform.position;
        while (true)
        {
            
            if ((dest - selectedObj.transform.position).magnitude < 0.1f)
            {
                if (selectedObj.name.Contains("Queen") || selectedObj.name.Contains("King"))
                {
                    anim = selectedObj.GetComponent<Animator>();
                    anim.SetBool("onMove", false);
                }
                selectedObj.transform.forward = Vector3.forward;
                selectedObj.transform.position = dest;
                if (selectedObj.GetComponent<Highlighter>())
                {
                    selectedObj.GetComponent<Highlighter>().constant = false;
                }
                else
                {
                    selectedObj.transform.GetChild(0).GetComponent<Highlighter>().constant = false;
                }

                BoardManager.Instance.selectedChessman = null;
                break;
            }
            if (selectedObj.name.Contains("Queen")|| selectedObj.name.Contains("King"))
            {
                anim = selectedObj.GetComponent<Animator>();
                anim.SetBool("onMove", true);
            }

            selectedObj.transform.rotation = Quaternion.Lerp(selectedObj.transform.rotation, Quaternion.LookRotation(dir), 0.1f);
            selectedObj.transform.position = Vector3.MoveTowards(selectedObj.transform.position,
                dest, 0.8f * Time.deltaTime);
            yield return null;
        }
    }
}


