﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkManager : MonoBehaviour
{
    Dictionary<int, string[]> talkData;

    void Awake()
    {
        talkData = new Dictionary<int, string[]>();
        GenerateData();
    }

    
    void GenerateData()
    {
        talkData.Add(1000, new string[] { "첫번째 관문: 플러피의 방.", "문을 막고 있는 흉폭한 곰을 음악으로 잠재우자." });

        talkData.Add(2000, new string[] { "두번째 관문: 열쇠방.", "문을 열 수 있는 단 하나의 진짜 열쇠를 잡아라." });

        talkData.Add(3000, new string[] { "세번째 관문: 거대 체스방.", "음성으로 체스말들을 진두지휘하며 게임을 승리로 이끌자." });
    }

    public string GetTalk(int id, int talkIndex)
    {
        if (talkIndex == talkData[id].Length)
            return null;
        else
            return talkData[id][talkIndex];
    }
}
