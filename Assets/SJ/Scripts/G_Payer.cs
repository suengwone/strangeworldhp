﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G_Payer : MonoBehaviour
{
    public GameManager manager;
    GameObject scanObject;

    // 공전 물체 만들기
    public GameObject gold;
    public GameObject talkText;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }


    void Update()
    {
        interActionButton();

        if (Input.GetButtonDown("Jnmp") && scanObject != null)
            manager.Action(scanObject);
    }


    void interActionButton()
    {
        if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
            //Touch touch = Input.GetTouch(0);
            Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << LayerMask.NameToLayer("Button")))
            {
                gold.SetActive(true);
                Destroy(hitInfo.transform.gameObject);
            }
        }
    }
}


    