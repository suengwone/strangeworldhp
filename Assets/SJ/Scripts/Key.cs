﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    // 열쇠 타입
    public enum Type { Gold, Silver };
    public Type type;
    public int value;

    Rigidbody cube;
    public float multi;

    private void Start()
    {
        StartCoroutine(MoveObject());
        cube = this.GetComponent<Rigidbody>();
        multi = 10;
    }
    IEnumerator MoveObject()
    {
        cube = GetComponent<Rigidbody>();

        while (true)
        {
            float dir1 = Random.Range(-1*multi, 1*multi);
            float dir2 = Random.Range(-1*multi, 1*multi);
            float dir3 = Random.Range(-1*multi, 1*multi);


            yield return new WaitForSeconds(0.8f);
            cube.velocity = new Vector3(dir1, dir2, dir3);
        }
    }
}
