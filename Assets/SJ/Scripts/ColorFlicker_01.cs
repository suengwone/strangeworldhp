﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorFlicker_01 : MonoBehaviour
{
    public Renderer lamp;
    private Color orginColor;

    // Start is called before the first frame update
    void Start()
    {
        // 램프 오브젝트의 색상정보를 저장한다.
        orginColor = lamp.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        // 사인파에 의한 시간변화를 곱하여 색 점멸.
        float flicker = Mathf.Abs(Mathf.Sin(Time.time * 1));
        lamp.material.color = orginColor * flicker;
    }
}
