﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start Sign UI
    public Text startSign;
    public Image start;

    public Text startSign2;
    public Image start2;

    public Text startSign3;
    public Image start3;



    public TalkManager talkManager;
    public GameObject talkPanel;
    public Text talkText;
    public GameObject scanObject;
    public bool isAction;
    public int talkIndex;

    public void Action(GameObject scanObj)
    {
        
        scanObject = scanObj;
        ObjData objData = scanObject.GetComponent<ObjData>();
        Talk(objData.id, objData.isNpc);

        talkPanel.SetActive(isAction); 
    }

    void Update()
    {
        // Start Sign UI를 실행한다.
        if(SceneManager.GetActiveScene().buildIndex == 1)
            StartSign();
        if(SceneManager.GetActiveScene().buildIndex == 2)
            StartSign2();
        if(SceneManager.GetActiveScene().buildIndex == 3)
            StartSign3();



    }

    private void StartSign()
    {
        if (Time.time > 2)
        {
            startSign.text = "문을 막고 있는 흉폭한 곰을 ";
        }
        if (Time.time > 5)
        {
            startSign.text = "음악으로 잠재우자.";
        }
        if (Time.time > 7)
        {
            startSign.gameObject.SetActive(false);
            start.gameObject.SetActive(false);
        }
    }

    private void StartSign2()
    {
        if (Time.time > 2)
        {
            startSign2.text = "문을 열 수 있는 단 하나의";
        }
        if (Time.time > 5)
        {
            startSign2.text = "진짜 열쇠를 찾아 방을 통과하자.";
        }
        if (Time.time > 7)
        {
            startSign2.gameObject.SetActive(false);
            start2.gameObject.SetActive(false);
        }
    }

    private void StartSign3()
    {
        if (Time.time > 2)
        {
            startSign3.text = "음성으로 체스말들을 진두지휘하며";
        }
        if (Time.time > 5)
        {
            startSign3.text = "게임을 승리로 이끌자.";
        }
        if (Time.time > 7)
        {
            startSign3.gameObject.SetActive(false);
            start3.gameObject.SetActive(false);
        }
    }






























    void Talk(int id, bool isNpc)
    {
        string talkData = talkManager.GetTalk(id, talkIndex);

        if (talkData == null)
        {
            isAction = false;
            talkIndex = 0;
            return;
        }

        if (isNpc)
        {
            talkText.text = talkData;
        }
        else
        {
            talkText.text = talkData;
        }

        isAction = true;
        talkIndex++;
    }
}
